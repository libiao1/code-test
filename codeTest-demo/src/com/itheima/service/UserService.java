package com.itheima.service;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.User;

import java.util.List;

public interface UserService {


    PageInfo<User> findByPage(Integer pageNum, Integer pageSize);

    /**
     * 查询所有用户
     *
     * @return
     */
    List<User> findAll();

    /**
     * 保存用户
     *
     * @param user
     */
    void save(User user);

    /**
     * 根据ID删除用户
     *
     * @param id
     */
    void deleteById(String id);

    /**
     * 根据id查询用户
     *
     * @param id
     * @return
     */
    User findById(String id);

    /**
     * 修改用户
     *
     * @param user
     */
    void update(User user);

    /**
     * 登录
     *
     * @param u
     * @return
     */
    User login(User u);

    User checkName(String name);
}
