package com.itheima.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.dao.UserDao;
import com.itheima.domain.User;
import com.itheima.service.UserService;
import com.itheima.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;

import java.util.List;


public class UserServiceImpl implements UserService {


    @Override
    public PageInfo<User> findByPage(Integer pageNum, Integer pageSize) {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserDao mapper = sqlSession.getMapper(UserDao.class);
        System.out.println("分页-----------------"+pageNum+"----------------"+pageSize);
        //设置分页参数
        PageHelper.startPage(pageNum, pageSize);
        //查询
        List<User> users = mapper.findAll();
        //封装PageInfo<User>
        PageInfo<User> pageInfo = new PageInfo<>(users);

        return pageInfo;
    }


    @Override
    public List<User> findAll() {
        //获取sqlSession对象
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        //获取mapper代理对象
        UserDao mapper = sqlSession.getMapper(UserDao.class);
        List<User> users = mapper.findAll();
        return users;
    }

    @Override
    public void save(User user) {
        //1.获取SqlSession对象
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        //2.获取Mapper代理对象
        UserDao mapper = sqlSession.getMapper(UserDao.class);
        //3.调用代理对象方法
        mapper.save(user);
        sqlSession.close();
    }

    @Override
    public void deleteById(String id) {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserDao mapper = sqlSession.getMapper(UserDao.class);
        mapper.deleteById(id);

    }

    @Override
    public User findById(String id) {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserDao mapper = sqlSession.getMapper(UserDao.class);
        User user = mapper.findById(id);

        return user;

    }

    @Override
    public void update(User user) {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserDao mapper = sqlSession.getMapper(UserDao.class);
      mapper.update(user);
    }

    @Override
    public User login(User u) {
        //1.获取SqlSession对象
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        //2.获取Mapper代理对象
        UserDao mapper = sqlSession.getMapper(UserDao.class);
        //3.调用mapper方法
        User user = mapper.login(u.getUsername(),u.getPassword());
        return user;
    }

    @Override
    public User checkName(String name) {
        return null;
    }


}
