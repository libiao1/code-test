package com.itheima.web.base;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;


public class BaseServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            //0. 设置请求编码, 设置在获取参数之前!!!
            request.setCharacterEncoding("UTF-8");
            response.setContentType("text/html;charset=utf-8");
            //1. 获取method参数标识
            String methodName = request.getParameter("method");
            //2. 反射方式实现: 动态的调用类的方法
            //1. 获取类的Class字节码对象
            Class clazz = this.getClass();
            //3. 获取方法对象
            Method method = clazz.getMethod(methodName, HttpServletRequest.class, HttpServletResponse.class);
            //4. 调用类的方法
            method.invoke(this,request,response);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
