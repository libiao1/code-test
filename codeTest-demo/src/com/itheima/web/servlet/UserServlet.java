package com.itheima.web.servlet;

import com.itheima.domain.User;
import com.itheima.service.UserService;
import com.itheima.service.impl.UserServiceImpl;
import com.itheima.web.base.BaseServlet;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.util.List;
import java.util.Map;

@WebServlet("/userServlet")
public class UserServlet extends BaseServlet {
    private UserService service = new UserServiceImpl();
    public void findAll(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //调用service方法
        List<User> userList = service.findAll();
        //存到请求域中
        request.setAttribute("userList", userList);
        //转发到list.jsp
        request.getRequestDispatcher("/list.jsp").forward(request, response);
    }

    public void findById(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String id = request.getParameter("id");
        List<User> userList = (List<User>) service.findById(id);
        request.setAttribute("userList", userList);
        request.getRequestDispatcher("/update.jsp").forward(request, response);
    }

    public void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            Map<String, String[]> map = request.getParameterMap();
            User user = new User();
            BeanUtils.populate(user, map);
            service.update(user);
            //5. 重定向到 userListServlet , 再次查询. 重定向以后, 浏览器路径发生了改变, 可以防止表单重复提交
            response.sendRedirect(request.getContextPath() + "/userServlet?method=findAll");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

