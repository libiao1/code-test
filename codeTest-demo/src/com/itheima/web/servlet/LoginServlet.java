package com.itheima.web.servlet;

import com.itheima.domain.User;
import com.itheima.service.UserService;
import com.itheima.service.impl.UserServiceImpl;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

@WebServlet("/loginServlet")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            //设置编码
            request.setCharacterEncoding("UTF-8");
            //获取参数并封装到User对象
            Map<String, String[]> map = request.getParameterMap();
            User u = new User();
            BeanUtils.populate(u,map);
            //调用service
            UserService service = new UserServiceImpl();
            User loginUser = service.login(u);
            //判断用户是否存在
            if (loginUser == null) {
                //不存在.就给出错误提示,再转发到login.jsp
                request.setAttribute("msg","用户名或密码错误");  //request域
                request.getRequestDispatcher("/login.jsp").forward(request,response);
            }else {
                //存在.保存用户信息,再重定向到index.jsp
                request.getSession().setAttribute("user",loginUser);  //session域
                response.sendRedirect(request.getContextPath()+"/index.jsp");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
