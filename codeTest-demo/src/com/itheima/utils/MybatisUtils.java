package com.itheima.utils;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class MybatisUtils {

    private MybatisUtils() {
    }

    private static SqlSessionFactory factory;

    static {
        try {
            InputStream in = Resources.getResourceAsStream("MybatisConfig.xml");
            factory = new SqlSessionFactoryBuilder().build(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 获取SqlSession对象
     *
     * @return
     */
    public static SqlSession getSqlSession() {
        return factory.openSession(true);
    }
}
