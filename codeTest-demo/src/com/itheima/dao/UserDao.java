package com.itheima.dao;

import com.itheima.domain.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface UserDao {
    List<User> findAll();

    @Insert("insert into user values(null,#{name},#{gender},#{age},#{address},#{qq},#{email},null,null)")
    void save(User user);
    @Delete("delete from student where id=#{id}")
    void deleteById(String id);
    @Select("select * from user where id=#{id}")
    User findById(String id);
    @Update("update user set name = #{name},gender = #{gender}, age= #{age} ,  address= #{address},  qq=#{qq},   email=#{email} where id= #{id}")
    void update(User user);

    @Select("select * from user where username = #{username} and password = #{password}")
    User login(@Param("username") String username, @Param("password") String password);

    User checkName(String name);
}
